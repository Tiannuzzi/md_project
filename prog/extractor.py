""" Program to extract and process OMIM gene data """

# Imports
import os, csv

# Move to appropriate directory
os.chdir("/Users/theodoreiannuzzi/Documents/md/master/data")

# Function definitions
def csv_extract(filename):
    array = []
    with open(filename) as datafile:
        df = csv.reader(datafile)
        for entry in df:
            array.append(entry)
    return array

def col_extract(filename, col):
    array = csv_extract(filename)
    col_data = []
    for entry in array:
        col_data.append(entry[col])
    return col_data

def csv_write(filename, array):
    with open(filename, "w+") as datafile:
        df = csv.writer(datafile)
        for entry in array:
            df.writerow(entry)

# Extract mitochondrial data
mito_list = sorted(col_extract("mito_genes.csv", 3))
print("All mitochondrial genes: {0}".format(len(mito_list)))

# Extract data from CSV files
ls = os.listdir()
files = [] 
for f in ls:
    if "omim" in f:
        files.append(f)

# Process data from file
def data_process(filename, array):

    # Extract data from file
    omim_arr = csv_extract(filename)

    # Filter for level of evidence
    filt_arr = []
    for entry in omim_arr:
        if entry[9] == "Pheno map key":
            filt_arr.append(entry)
        elif entry[9] == "3" and entry[1] != filt_arr[-1][1]:
            filt_arr.append(entry)

    # Filter for mitochondrial expression
    new_arr = []
    for entry in filt_arr:
        for gene in array:
            if gene in entry[2] or entry[9] == "Pheno map key":
                new_arr.append(entry)
                break

    # Quality control
    print("{0}:\n  OMIM: {1}\n  Filt: {2}\n  Mito: {3}".
        format(filename, len(omim_arr), len(filt_arr), len(new_arr))
        )

    # Write to new file
    new_file = filename.split("_")[0] + ".csv"
    csv_write(new_file, new_arr)

mito_data = {}

for f in files:
    data_process(f, mito_list)
    mito_data[f] = csv_extract(f)

def combine(file1, file2):
    f1 = csv_extract(file1)
    f2 = csv_extract(file2)
    combo = f1
    for g2 in f2:
        count = 1
        for g1 in f1:
            if g2[1] == g1[1]:
                count = 0
        if count:
            combo.append(g2)
    
    newfile = "{0}_{1}.csv".format(file1.split(".")[0], file2.split(".")[0])
    print("{0}: {1}".format(newfile, len(combo)))
    csv_write(newfile, combo)

combine("enc.csv", "epi.csv")
combine("enc.csv", "leu.csv")
combine("epi.csv", "leu.csv")
combine("als.csv", "ftd.csv")

fin_dat = col_extract("enc_epi.csv", 2)
fin_dat.pop(0)
fin_dat = sorted(fin_dat)
new_dat = [["Gene/Locus",""]]
for f in range(len(fin_dat)):
    new_dat.append(fin_dat[f].split(", "))
csv_write("fin.csv", new_dat)
