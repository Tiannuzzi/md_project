""" Program to sort variants """

import csv, os, sys

# Move to appropriate directory
os.chdir("/Users/theodoreiannuzzi/Documents/md/master/data")

# Function definitions
def csv_extract(filename):
    array = []
    with open(filename) as datafile:
        df = csv.reader(datafile)
        for entry in df:
            array.append(entry)
    return array

def col_extract(filename, col):
    array = csv_extract(filename)
    col_data = []
    for entry in array:
        col_data.append(entry[col])
    return col_data

def csv_write(filename, array):
    with open(filename, "w+") as datafile:
        df = csv.writer(datafile)
        for entry in array:
            df.writerow(entry)

all_vars = csv_extract("mito_vars.csv")

exac_filt_vars = []

for i in all_vars:
    if i[18] == ".":
        exac_filt_vars.append(i)
    else:
        z = 1
        try: 
            z = float(i[18])
        except ValueError:
            pass
        if z < 0.0001:
            exac_filt_vars.append(i)

print("exac", len(exac_filt_vars))

gp_filt_vars = []

for i in exac_filt_vars:
    if i[20] == ".":
        gp_filt_vars.append(i)
    else:
        z = 1
        try: 
            z = float(i[20])
        except ValueError:
            pass
        if z < 0.0001:
            gp_filt_vars.append(i)

print("gp", len(gp_filt_vars))

ac_filt_vars = []

for i in gp_filt_vars:
    if int(i[3].split(",")[0]) < 20:
        ac_filt_vars.append(i)

print("ac", len(ac_filt_vars))

imp_filt_vars = []

for i in ac_filt_vars:
    if "MODERATE" in i[16]:
        imp_filt_vars.append(i)
    elif "HIGH" in i[16]:
        imp_filt_vars.append(i)

print("imp", len(imp_filt_vars))

eff_filt_vars = []

effects = ["missense_variant", "frameshift_variant",
    "disruptive_inframe_deletion", "disruptive_inframe_insertion", 
    "start_lost", "stop_gained", "protein_protein_contact",
    "splice_acceptor_variant", "splice_donor_variant", "inframe_deletion", 
    "inframe_insertion"
    ]

for i in imp_filt_vars:
    for e in effects:
        if e in i[15]:
            eff_filt_vars.append(i)
            break

print("eff", len(eff_filt_vars))

mark_filt_vars = []

mark_list = ["APOPT1", "AUH", "COA7", "GCDH", "IBA57",
    "IDH2", "L2HGDH", "LMNB1", "LYRM7", "NAXE", "NDUFA13",
    "NDUFAF2", "NDUFAF3", "NDUFAF5", "NDUFS1", "NOTCH3",
    "NUBPL", "RPIA", "SDHA", "TACO1", "WARS2"]

for i in eff_filt_vars:
    c = 0
    for e in mark_list:
        if e in i[9]:
            c += 1
    if c == 0:
        mark_filt_vars.append(i)

print("markless", len(mark_filt_vars))

file_names = ["exac_vars.csv", "gp_vars.csv", "ac_vars.csv", 
    "imp_vars.csv", "eff_vars.csv", "markless_vars.csv"]

arrays = [exac_filt_vars, gp_filt_vars, ac_filt_vars,
    imp_filt_vars, eff_filt_vars, mark_filt_vars]

for i in range(len(file_names)):
    csv_write(file_names[i], arrays[i])


